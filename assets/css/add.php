<?php
	session_start();
	include ("bd.php");
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Strata by HTML5 UP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body id="top">

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="//an-kian.fcknawsm.ru/" class="image avatar"><img src="images/logo.png" alt="" /></a>
					<h1>Просмотр объявлений:</h1>
					<ul class="header-operations">
						<a href="//an-kian.fcknawsm.ru/sell"><li>Продажа</li></a>
						<a href="//an-kian.fcknawsm.ru/rent"><li>Аренда</li></a>
						<a href=""><li>Обмен</li></a>
						<a href="//an-kian.fcknawsm.ru/buy"><li>Покупка</li></a>
					</ul>
					<?php if (empty($_SESSION['email'])) {  ?>
						<a href="//an-kian.fcknawsm.ru/login"><span id="login-btn">Войти</span></a>
						<a href="//an-kian.fcknawsm.ru/reg"><span id="add-btn">Регистрация</span></a>
				    <?php   } else {   ?>
						<span id="add2-btn" onclick="location.href='//an-kian.fcknawsm.ru/add'">Добавить объявление</span>
					 <?php if($_SESSION['email'] == "admin") {   ?>
						<span id="admin-btn" onclick="location.href='//an-kian.fcknawsm.ru/admin'">Админка</span>
					<?php } ?>
						<div class="login-as">Вы вошли как<br><?php echo $_SESSION['email'] ?></div>
				   <?php }    ?>
				</div>
			</header>
			<?php
				$type = $_GET['type'];
				$objType =$_GET['objType'];

				$login = $_SESSION['email'];
				$getCodeX = mysql_query("SELECT codeX FROM `users` WHERE `email` = '".$login."';");
  				$codeX = mysql_result($getCodeX,0);
			?>
			<style type="text/css">
				:root {
  				--add-color: <?php echo "#".$type ?>;
  				}
  				.<?php echo $objType ?> {
  					background: var(--add-color);
  					color: #fff !important;
  					border-color:  var(--add-color) !important;
  				}
  				body {
  					overflow: scroll;
  				}
			</style>
			
		<!-- Main -->
			<div id="main-header">
				<ul class="menu">
					<a class="menu-parent" href="//an-kian.fcknawsm.ru"><li>Главная</li></a>
					<?php if (empty($_SESSION['email'])) {  ?>
					<a class="menu-parent" href="//an-kian.fcknawsm.ru/login"><li>Профиль</li></a>
					<?php   } else {   ?>
					<div class="dropdown">
					  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Профиль<span class="caret"></span></button>
					  <ul class="dropdown-menu">
					    <li><a href="//an-kian.fcknawsm.ru/my">Мои объявления</a></li>
					    <li><a href="//an-kian.fcknawsm.ru/profile">Изменить данные</a></li>
					    <li><a href="//an-kian.fcknawsm.ru/logout">Выйти из аккаунта</a></li>
					  </ul>
					</div>
					<?php }   ?>
					<a class="menu-parent" href=""><li>Инвестиции</li></a>
					<a class="menu-parent" href="//an-kian.fcknawsm.ru/contact"><li>Контакты</li></a>
				</ul>
			</div>
			
			<div id="main">

				<!-- One -->
					<section id="one add">
						<header class="major">
							<h2>Добавить объявление</h2>
						</header>
						<?php if (!$type) { ?>
						<div id="add-step1">
							<p id="add-step1-p">Выберите тип операции</p>
							<div class="add-type add-step1">
								<a href="//an-kian.fcknawsm.ru/add?type=E91E63">Продажа</a>
								<a href="//an-kian.fcknawsm.ru/add?type=3F51B5">Аренда</a>
								<a href="//an-kian.fcknawsm.ru/add?type=009688">Обмен</a>
								<a href="//an-kian.fcknawsm.ru/add?type=FF9800">Покупка</a>
							</div>
						</div>
						<?php } ?>
						<?php if ($type == "E91E63" || $type == "3F51B5" || $type == "009688" || $type == "FF9800") { ?>
						 	<div id="add-step2">
							<?php if ($type == "E91E63") {echo "<p id='add-step2-p' style='color:var(--add-color)'>Продам</p>";} ?>
							<?php if ($type == "3F51B5") {echo "<p id='add-step2-p' style='color:var(--add-color)'>Сдам</p>";} ?>
							<?php if ($type == "009688") {echo "<p id='add-step2-p' style='color:var(--add-color)'>Обменяю</p>";} ?>
							<?php if ($type == "FF9800") {echo "<p id='add-step2-p' style='color:var(--add-color)'>Куплю</p>";} ?>
								<div class="add-type add-step2">
									<a href="//an-kian.fcknawsm.ru/add?type=<?php echo $type ?>&objType=kv" class="kv">Квартиру</a>
									<a href="//an-kian.fcknawsm.ru/add?type=<?php echo $type ?>&objType=dm" class="dm">Дом</a>
									<a href="//an-kian.fcknawsm.ru/add?type=<?php echo $type ?>&objType=km" class="km">Комнату</a>
									<a href="//an-kian.fcknawsm.ru/add?type=<?php echo $type ?>&objType=grj" class="grj">Гараж</a>
								</div>
							</div>
						<?php } ?>
							
						
						<?php if ($objType) { ?>
						<div id="add-step3">
						<p id="add-step3-p">Заполните параметры</p>

						<form id="addForm" enctype="multipart/form-data" method="post" name="test" action="connect">
							<div class="main-deatils">
							 	<div class="field">
							 		<p>Населённый пункт</p>
							 		<select size="1" name="city">
										<OPTION value="Камышин">Камышин</OPTION>
										<OPTION value="Петров Вал">Петров Вал</OPTION>
										<OPTION value="Александровка">Александровка</OPTION>
										<OPTION value="Антиповка">Антиповка</OPTION>
										<OPTION value="Барановка">Барановка</OPTION>
										<OPTION value="Белогорки">Белогорки</OPTION>
										<OPTION value="Бутковка">Бутковка</OPTION>
										<OPTION value="Верхняя Грязнуха">Верхняя Грязнуха</OPTION>
										<OPTION value="Верхняя Добринка">Верхняя Добринка</OPTION>
										<OPTION value="Верхняя Куланинка">Верхняя Куланинка</OPTION>
										<OPTION value="Верхняя Липовка">Верхняя Липовка</OPTION>
										<OPTION value="Веселово">Веселово</OPTION>
										<OPTION value="Вихлянцево">Вихлянцево</OPTION>
										<OPTION value="Воднобуерачное">Воднобуерачное</OPTION>
										<OPTION value="Галка">Галка</OPTION>
										<OPTION value="Госселекстанция">Госселекстанция</OPTION>
										<OPTION value="Грязнуха">Грязнуха</OPTION>
										<OPTION value="Гуселка">Гуселка</OPTION>
										<OPTION value="Дворянское">Дворянское</OPTION>
										<OPTION value="Дубовка">Дубовка</OPTION>
										<OPTION value="Ельшанка">Ельшанка</OPTION>
										<OPTION value="Зеленый Гай">Зеленый Гай</OPTION></OPTION>
										<OPTION value="Ионов хутор">Ионов хутор</OPTION>
										<OPTION value="Калиновка">Калиновка</OPTION>
										<OPTION value="Карпунин">Карпунин</OPTION>
										<OPTION value="Костарево">Костарево</OPTION>
										<OPTION value="Лебяжье">Лебяжье</OPTION>
										<OPTION value="Мичуринский поселок">Мичуринский поселок</OPTION>
										<OPTION value="Нагорный хутор">Нагорный хутор</OPTION>
										<OPTION value="Нижняя Добринка">Нижняя Добринка</OPTION>
										<OPTION value="Нижняя Липовка">Нижняя Липовка</OPTION></OPTION>
										<OPTION value="Пановка">Пановка</OPTION>
										<OPTION value="Петрунино">Петрунино</OPTION>
										<OPTION value="Поповка">Поповка</OPTION>
										<OPTION value="Саломатино село">Саломатино село</OPTION>
										<OPTION value="Саломатино жд станция">Саломатино жд станция</OPTION>
										<OPTION value="Семеновка">Семеновка</OPTION>
										<OPTION value="Средняя Камышинка">Средняя Камышинка</OPTION>
										<OPTION value="Таловка">Таловка</OPTION>
										<OPTION value="Терновка">Терновка</OPTION>
										<OPTION value="Умет">Умет</OPTION>
										<OPTION value="УстьГрязнуха">УстьГрязнуха</OPTION>
										<OPTION value="Фермы №3 совхоза Добринский">Фермы №3 совхоза Добринский</OPTION>
										<OPTION value="Чухонастовка">Чухонастовка</OPTION>
										<OPTION value="Щербаковка">Щербаковка</OPTION></OPTION>
										<OPTION value="Щербатовка">Щербатовка</OPTION>
										<OPTION value="Другое">Другое</OPTION>
								  	</select>
							 	</div>
							 	<?php if($objType != "grj") { ?>
							 	<div class="field3">
							 		<div class="field31"><p>Адрес (мкр,улица и пр.)</p> <input type="" name="street"></div>
							 		<?php if($type != "FF9800") { ?>
							 		<div class="field31"><p>№ дома</p> <input type="" name="numHouse"></div>
							 		<?php if($objType != "dm") { ?>
							 		<div class="field31"><p>№ квартиры</p> <input type="" name="numFlat"></div>
							 		<?php } }?>
							 	</div>
							 	<div class="field field-detail"><p>Количество комнат</p><input type="" name="countRooms"></div>
							 	<div class="field3">
							 		<div class="field field31 field-detail"><p>Тип дома</p>
								 		<select size="1" name="houseType">
									 		<option value="панельный">панельный</option>
									 		<option value="кирпичный">кирпичный</option>
									 		<option value="шлако-блочный">шлако-блочный</option>
									 		<option value="деревянный">деревянный</option>
									 		<option value="каркасный">каркасный</option>
								 		</select>
									</div>
						 		</div>
						 		<?php if($objType != "dm") { ?>
							 	<div class="field field-detail"><p>Этаж</p><input type="" name="floor"></div>
							 	<?php } ?>
							 	<?php if($type != "FF9800") { ?>
							 	<div class="field field-detail"><p>Этажей в доме</p><input type="" name="allFloor"></div>
							 	<div class="field field-detail area"><p>Общая площадь</p><input type="" name="allS"><span>м²</span></div>
							 	<div class="field field-detail area"><p>Площадь кухни</p><input type="" name="kitchenS"><span>м²</span></div>
							 	<div class="field field-detail area"><p>Жилая площадь</p><input type="" name="realS"><span>м²</span></div>
							 	<?php if($objType == "dm") { ?>
							 	<div class="field field-detail area"><p>Площадь участка</p><input type="" name="numFlat"><span>м²</span></div>
							 	<?php } } ?>
							 	<?php if($objType != "dm") { ?>
							 	<!-- <div class="field field-detail"><p></p><input type="" name=""></div> -->
							 	<input type="checkbox" name="balcony" value="1"> Балкон
							 	<input type="checkbox" name="loggia" value="1"> Лоджия
							 	<?php } ?>
							 	
							 	<?php } else { ?>

							 	<div class="field">
							 		<p>Адрес (мкр,улица и пр.)</p> <input type="" name="street">
							 	</div>
							 	<div class="field"><p>№ бокса</p> <input type="" name="numHouse"></div>
							 	<div class="field3">
							 		<div class="field field31 field-detail"><p>Тип гаража</p>
								 		<select size="1" name="houseType">
									 		<option value="панельный">панельный</option>
									 		<option value="кирпичный">кирпичный</option>
									 		<option value="шлако-блочный">шлако-блочный</option>
									 		<option value="деревянный">деревянный</option>
									 		<option value="каркасный">каркасный</option>
								 		</select>
									</div>
						 		</div>
						 		<div class="field field-detail area"><p>Общая площадь</p><input type="" name="allS"><span>м²</span></div>


							 	<?php } ?>


						 	</div>
						 	<div id="border"></div>
						 	
						 	<div class="field field-detail"><p>Описание объявления</p><textarea name="description"></textarea></div>
						 	<?php if($type != "FF9800") { ?>
						 	<div class="field3">
							 	<div class="field field31 field-detail"><p>Тип объекта</p>
							 		<select size="1" name="type">
								 		<option value="Вторичка">на вторичном рынке</option>
								 		<option value="Новостройка">в новом доме</option>
								 		<option value="Улучш. планировка">улучш.планировки</option>
								 		<option value="С частич. удобст.">с частич.удобст.</option>
								 		<option value="Другое">другое</option>
							 		</select>
								</div>
						 	</div>
							<div class="field3">
							 	<div class="field field31 field-detail area"><p>Цена</p><input type="" name="price"><span>₽</span></div>
						 	</div>
						 	<div id="pics">
							 	<div class="field"><input type="file" name="picture1"></div>
						 	</div>
						 	<div class="field"><span id="addPic" onclick="javascript:addPic()">+ Еще изображение</span></div>
						 	<?php } ?>

						 	<input type="" name="user" value="<?php echo $login ?>" hidden>
						 	<input type="" name="codeX" value="<?php echo $codeX ?>" hidden>
						 	<input type="" name="objType" value="<?php echo $objType ?>" hidden>
						 	<input type="" name="type" value="<?php echo $type ?>" hidden>

							<input type="submit" name="submitFlat" class="orng-btn" value="Добавить">
						</form>

						</div>
						<?php } ?>
					</section>
			</div>

		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<ul class="copyright">
						<li>&copy;An-kian — сайт объявлений. Использование сайта, в том числе подача объявлений, означает согласие с пользовательским соглашением.</li>
					</ul>
				</div>
			</footer>
			<script type="text/javascript">
				var picNum=1;
				var pics = document.getElementById('pics');

				function addPic(i) {
					if (picNum < 10) {
						picNum+=1;
						pics.innerHTML += '<div class="field"><input type="file" name="picture'+ picNum + '"></div>';
						if (picNum == 10) {
							document.getElementById('addPic').innerHTML = "";
						}
					}
				}
				

			</script>
			
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>